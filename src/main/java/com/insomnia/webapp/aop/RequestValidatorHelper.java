package com.insomnia.webapp.aop;

import java.lang.reflect.Array;
import java.util.Collection;

import com.insomnia.webapp.common.operationresult.ResponseMessage;
import com.insomnia.webapp.exceptions.ValidationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.multipart.MultipartFile;

public class RequestValidatorHelper {
    private static final String VALIDATE_EMAIL_PATTERN = "^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)+$";

    public static void validateId(Long value, String fieldName) {
        if (value == null || value < 0) {
            throw new ValidationException(ResponseMessage.businessError("common.error.idIsNotValid", fieldName));
        }
    }

    public static void usernameValidator(String value, String messageCode, String fieldName) {
        validateNullability(value, messageCode, fieldName);
        validateEmail(value, messageCode, fieldName);
    }

    private static void validateEmail(String value, String messageCode, String fieldName) {
        if (!validateEmail(value.trim())) {
            throw new ValidationException(ResponseMessage.businessError(messageCode, fieldName));
        }
    }

    private static boolean validateEmail(String email) {
        return email.matches(VALIDATE_EMAIL_PATTERN);
    }

    public static void validateNullability(Object value, String messageCode, String fieldName) {
        if (value == null || isBlankString(value) || isEmptyCollection(value) || isEmptyArray(value) || isEmptyMultipartFile(value)) {
            throw new ValidationException(ResponseMessage.businessError(messageCode, fieldName));
        }
    }

    private static boolean isBlankString(Object object) {
        return object instanceof String && StringUtils.isBlank((String) object);
    }

    private static boolean isEmptyCollection(Object object) {
        return object instanceof Collection && ((Collection) object).isEmpty();
    }

    private static boolean isEmptyArray(Object value) {
        return value.getClass().isArray() && Array.getLength(value) == 0;
    }

    private static boolean isEmptyMultipartFile(Object value) {
        return value instanceof MultipartFile && ((MultipartFile) value).isEmpty();
    }
}
