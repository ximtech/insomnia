package com.insomnia.webapp.aop;

import com.insomnia.webapp.common.operationresult.ResponseMessage;
import com.insomnia.webapp.exceptions.ValidationException;
import com.insomnia.webapp.services.LocalizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {

    private LocalizationService localizationService;

    @Autowired
    public AppExceptionHandler(LocalizationService localizationService) {
        this.localizationService = localizationService;
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    public @ResponseBody ResponseMessage handleInvalidRequest(ValidationException exception, ServletWebRequest request) {
        ResponseMessage responseMessage = exception.getResponseMessage();
        localizationService.setLocalizedMessageToResponse(responseMessage);
        DataLogger.logErrorMessage(exception, responseMessage);
        return responseMessage;
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody ResponseMessage handleInvalidRequest(Exception exception, ServletWebRequest request) {
        ResponseMessage responseMessage = ResponseMessage.internalException();
        localizationService.setLocalizedMessageToResponse(responseMessage);
        DataLogger.logErrorMessage(exception, responseMessage);
        return responseMessage;
    }
}
