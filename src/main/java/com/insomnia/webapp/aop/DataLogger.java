package com.insomnia.webapp.aop;

import java.util.StringJoiner;
import java.util.stream.Stream;

import com.insomnia.webapp.common.operationresult.ResponseMessage;
import com.insomnia.webapp.common.operationresult.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DataLogger {
    private static final Logger LOGGER = LogManager.getLogger(DataLogger.class);

    private static final String[] SCHEDULER_PARAMS = { "core.pool.size=", "max.pool.size=", "keep.alive.time=", "queue.max.size=" };

    public static void logErrorMessage(Exception exception, ResponseMessage responseMessage) {
        String exceptionName = exception.getClass().getSimpleName();
        Status responseStatus = responseMessage.getStatus();
        String localizedErrorMessage = responseMessage.getMessage();
        LOGGER.error(String.format("%1$s. Status: %2$s Message: %3$s", exceptionName, responseStatus, localizedErrorMessage));
        exception.printStackTrace();
    }

    public static void logRequestStart(Object request, String userName) {
        LOGGER.info(String.format("Started request %s, invoked by %s", request.toString(), userName));
    }

    public static void logRequestEnd(Object request, long endTime) {
        String className = request.getClass().getSimpleName();
        LOGGER.info(String.format("Request with name: %s finished in %d ms", className, endTime));
    }

    public static void logSchedulerParameters(String schedulerName, int... values) {
        String prefix = "Creating " + schedulerName + " scheduler: ";
        StringJoiner joiner = new StringJoiner(", ", prefix, "");
        Stream.iterate(0, i -> i + 1)
                        .limit(SCHEDULER_PARAMS.length)
                        .forEach((Integer i) -> joiner.add(SCHEDULER_PARAMS[i] + values[i]));
        LOGGER.info(joiner.toString());
    }
}
