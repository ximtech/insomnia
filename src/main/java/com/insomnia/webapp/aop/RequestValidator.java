package com.insomnia.webapp.aop;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import com.insomnia.webapp.aop.annotations.BlankFieldValidator;
import com.insomnia.webapp.aop.annotations.IdValidator;
import com.insomnia.webapp.aop.annotations.UserNameValidator;

public class RequestValidator {
    private static final Map<Class<?>, ValidatorType> verifiersMap;

    static {
        verifiersMap = new HashMap<>();
        verifiersMap.put(BlankFieldValidator.class, ValidatorType.BLANK_FIELD_VALIDATOR);
        verifiersMap.put(IdValidator.class, ValidatorType.ID_VALIDATOR);
        verifiersMap.put(UserNameValidator.class, ValidatorType.USER_NAME_VALIDATOR);
    }

    enum ValidatorType {
        BLANK_FIELD_VALIDATOR, ID_VALIDATOR, USER_NAME_VALIDATOR
    }

    public static void validateRequest(Object request) {
        Field[] fields = request.getClass().getDeclaredFields();
        Arrays.stream(fields).forEach((Field field) -> {
                    Annotation[] fieldAnnotations = field.getDeclaredAnnotations();
                    validateRequestParameters(request, fieldAnnotations, field); });
    }

    private static void validateRequestParameters(Object request, Annotation[] fieldAnnotations, Field field) {
        Arrays.stream(fieldAnnotations).forEach((Annotation annotation) -> {
            ValidatorType validatorType = verifiersMap.get(annotation.annotationType());
            if (validatorType != null) {
                resolveAnnotation(validatorType, annotation, request, field);
            } });
    }

    private static void resolveAnnotation(ValidatorType validatorType, Annotation annotation, Object request, Field field) {
        Class<?> beanClass = request.getClass();
        Object value = getFieldValue(request, field, beanClass);

        switch (validatorType) {
            case BLANK_FIELD_VALIDATOR:
                BlankFieldValidator blankFieldValidator = (BlankFieldValidator) annotation;
                RequestValidatorHelper.validateNullability(value, blankFieldValidator.messageCode(), blankFieldValidator.fieldName());
                break;
            case ID_VALIDATOR:
                IdValidator idValidator = (IdValidator) annotation;
                RequestValidatorHelper.validateId((Long) value, idValidator.fieldName());
                break;
            case USER_NAME_VALIDATOR:
                UserNameValidator userNameValidator = (UserNameValidator) annotation;
                RequestValidatorHelper.usernameValidator((String) value, userNameValidator.messageCode(), userNameValidator.fieldName());
                break;
            default: throw new IllegalArgumentException("Unknown validator type");
        }
    }

    private static Object getFieldValue(Object request, Field field, Class<?> beanClass) {
        try {
            return new PropertyDescriptor(field.getName(), beanClass).getReadMethod().invoke(request);
        } catch (IllegalAccessException | InvocationTargetException | IntrospectionException e) {
            throw new RuntimeException(e);
        }
    }
}
