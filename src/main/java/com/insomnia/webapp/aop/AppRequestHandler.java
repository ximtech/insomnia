package com.insomnia.webapp.aop;

import com.insomnia.webapp.common.operationresult.ProcessingResult;
import com.insomnia.webapp.services.LocalizationService;
import com.insomnia.webapp.services.interfaces.SpringSecurityService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AppRequestHandler {

    private SpringSecurityService securityService;
    private LocalizationService localizationService;

    @Autowired
    public AppRequestHandler(SpringSecurityService securityService,
                                LocalizationService localizationService) {
        this.securityService = securityService;
        this.localizationService = localizationService;
    }

    @Pointcut("within(@org.springframework.stereotype.Controller *) || " +
             "within(@org.springframework.web.bind.annotation.RestController *)")
    public void controllerClasses() {}

    @Pointcut("execution (public * "
                    + "com.insomnia.webapp.*.*.*"
                    + "(@org.springframework.web.bind.annotation.RequestBody "
                    + "(@com.insomnia.webapp.aop.annotations.Request *)))")
    public void onlyMethodsWithRequestAnnotation() {}

    @Around(value = "controllerClasses() && onlyMethodsWithRequestAnnotation()")
    public Object logRequestData(ProceedingJoinPoint joinPoint) throws Throwable {
        Object request = joinPoint.getArgs()[0];
        long start = System.currentTimeMillis();

        String userName = securityService.getCurrentUserName();
        DataLogger.logRequestStart(request, userName);
        RequestValidator.validateRequest(request);

        Object result = joinPoint.proceed();

        long end = System.currentTimeMillis();
        long endTime = end - start;
        DataLogger.logRequestEnd(request, endTime);

        return result;
    }

    @Pointcut("execution (public (@com.insomnia.webapp.aop.annotations.Response *) "
                    + "com.insomnia.webapp.*.*.*((@com.insomnia.webapp.aop.annotations.Request *)))")
    public void allMethodsWithResponse() {}

    @AfterReturning(value = "allMethodsWithResponse()", returning = "responseObject")
    public Object processResolvableMessage(Object responseObject) {
        ProcessingResult processingResult = (ProcessingResult) responseObject;
        localizationService.setLocalizedMessageToResponse(processingResult.getResponseMessage());
        return responseObject;
    }

}

