package com.insomnia.webapp.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private UserDetailsService userDetailsService;
    private AuthenticationSuccessHandler successHandler;
    private PasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public SecurityConfig(UserDetailsService userDetailsService,
                    AuthenticationSuccessHandler successHandler,
                    PasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.successHandler = successHandler;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Autowired
    public void registerGlobalAuthentication(AuthenticationManagerBuilder authenticationBuilder) throws Exception {
        authenticationBuilder
                        .userDetailsService(userDetailsService)
                        .passwordEncoder(bCryptPasswordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf()
                        .requireCsrfProtectionMatcher(new AntPathRequestMatcher("**/login"))
                        .and()
                        .authorizeRequests()
                        .antMatchers("/index").hasRole("USER")
                        .and();

        http.formLogin()
                        .successHandler(successHandler)
                        .loginPage("/login")
                        .usernameParameter("j_username")
                        .passwordParameter("j_password")
                        .and();

        http.logout()
                        .permitAll()
                        .invalidateHttpSession(true);
    }
}
