package com.insomnia.webapp.security;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

@Component
public class SecurityAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy;

    @Autowired
    public SecurityAuthenticationSuccessHandler(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest arg0,
                                        HttpServletResponse arg1,
                                        Authentication authentication) throws IOException {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for (GrantedAuthority authority : authorities) {
            switch (UserRole.getRoleUserAuthority(authority.getAuthority())) {
                case ROLE_USER:
                    redirectStrategy.sendRedirect(arg0, arg1, "/index");
                    break;
                case ROLE_ADMIN:
                    redirectStrategy.sendRedirect(arg0, arg1, "/admin");
                    break;
                case ROLE_ANONYMOUS:
                    break;
                default:
                    throw new IllegalStateException("Unmapped role type");
            }
        }
    }

}
