package com.insomnia.webapp.security;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_USER,
    ROLE_ANONYMOUS;

    public static UserRole getRoleUserAuthority(String authority) {
        for (UserRole role : UserRole.values()) {
            if (role.name().equals(authority)) {
                return role;
            }
        }
            return ROLE_ANONYMOUS;
    }
}
