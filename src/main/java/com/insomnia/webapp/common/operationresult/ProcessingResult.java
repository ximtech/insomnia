package com.insomnia.webapp.common.operationresult;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProcessingResult {
    private ResponseMessage responseMessage = ResponseMessage.success();

    @JsonIgnore
    public Status getStatus() {
        return responseMessage.getStatus();
    }

    @JsonIgnore
    public Type getType() {
        return responseMessage.getType();
    }

    @JsonProperty
    public ResponseMessage getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(ResponseMessage responseMessage) {
        this.responseMessage = responseMessage;
    }
}
