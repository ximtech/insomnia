package com.insomnia.webapp.common.operationresult;

public enum Type {
    SUCCESS,
    ERROR
}
