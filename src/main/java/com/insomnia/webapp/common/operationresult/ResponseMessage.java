package com.insomnia.webapp.common.operationresult;

import static com.fasterxml.jackson.annotation.JsonInclude.Include;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.springframework.context.support.DefaultMessageSourceResolvable;

@JsonPropertyOrder({ "type", "status", "message", "fieldName" })
public class ResponseMessage {

    @JsonProperty
    private Type type;

    @JsonProperty
    private Status status;

    @JsonProperty
    private String message;

    @JsonIgnore
    private DefaultMessageSourceResolvable sourceResolvable;

    @JsonInclude(Include.NON_NULL)
    private String fieldName;

    private ResponseMessage(Type type, Status status, String code) {
        this(type, status, code, new Object[0]);
    }

    private ResponseMessage(Type type, Status status, String code, Object[] args) {
        this(type, status, code, null, args);
    }

    private ResponseMessage(Type type, Status status, String code, String fieldName, Object[] args) {
        this.status = status;
        this.type = type;
        this.fieldName = fieldName;
        this.sourceResolvable = new DefaultMessageSourceResolvable(new String[]{code}, args);
    }

    public static ResponseMessage success() {
        return success("common.success.dataSaveOk");
    }

    public static ResponseMessage success(String i18nCode) {
        return new ResponseMessage(Type.SUCCESS, Status.OK, i18nCode);
    }

    public static ResponseMessage businessError(String i18nCode) {
        return businessError(i18nCode, null, null);
    }

    public static ResponseMessage businessError(String i18nCode, String fieldId) {
        return businessError(i18nCode, fieldId, null);
    }

    public static ResponseMessage businessError(String i18nCode, String fieldId, Object[] args) {
        return new ResponseMessage(Type.ERROR, Status.UNPROCESSABLE_ENTITY, i18nCode, fieldId, args);
    }

    public static ResponseMessage notFound() {
        return new ResponseMessage(Type.ERROR, Status.NOT_FOUND, "common.label.dataNotFound");
    }

    public static ResponseMessage internalException() {
        return new ResponseMessage(Type.ERROR, Status.INTERNAL_SERVER_ERROR, "common.error.defaultErrorMessage");
    }

    public static ResponseMessage forbidden(String i18nCode) {
        return forbidden(i18nCode, null);
    }

    public static ResponseMessage forbidden(String i18nCode, Object[] args) {
        return new ResponseMessage(Type.ERROR, Status.FORBIDDEN, i18nCode, args);
    }

    public static ResponseMessage unauthorized(String i18nCode, Object[] args) {
        return new ResponseMessage(Type.ERROR, Status.UNAUTHORIZED, i18nCode, args);
    }

    public Type getType() {
        return type;
    }

    public Status getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DefaultMessageSourceResolvable getSourceResolvable() {
        return sourceResolvable;
    }

    public String getFieldName() {
        return fieldName;
    }
}
