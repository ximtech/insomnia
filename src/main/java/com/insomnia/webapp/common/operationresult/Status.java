package com.insomnia.webapp.common.operationresult;

public enum Status {
    OK,
    UNPROCESSABLE_ENTITY,
    FORBIDDEN,
    INTERNAL_SERVER_ERROR,
    NOT_FOUND,
    UNAUTHORIZED
}
