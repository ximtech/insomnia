package com.insomnia.webapp.configuration;

import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.insomnia.webapp.aop.DataLogger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import org.springframework.ui.freemarker.FreeMarkerConfigurationFactoryBean;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Configuration
@PropertySource("${application.config}")
public class MailConfig {

    @Value("${mail.scheduler.core.pool.size: 2}")
    private int mailSchedulerCorePoolSize;
    @Value("${mail.scheduler.max.pool.size: 5}")
    private int mailSchedulerMaxPoolSize;
    @Value("${mail.scheduler.keep.alive.time.secs: 60}")
    private int mailSchedulerKeepAliveTime;
    @Value("${mail.scheduler.queue.max.size: 100}")
    private int mailSchedulerQueueMaxSize;

    @Value("${mail.host: localhost}")
    private String mailHost;
    @Value("${mail.port: 25}")
    private int mailPort;
    @Value("${mail.username}")
    private String mailUsername;
    @Value("${mail.password}")
    private String mailPassword;
    @Value("${mail.transport.protocol: smtp}")
    private String transportProtocol;
    @Value("${mail.smtp.auth: false}")
    private String mailSmtpAuth;
    @Value("${mail.smtp.starttls.enable: false}")
    private String mailSmtpStartTlsEnable;
    @Value("${mail.debug: false}")
    private String mailDebug;

    @Value("${mail.template.location: classpath:templates/}")
    private String templateLocation;

    @Bean
    @Qualifier("mail-scheduler")
    public Scheduler emailScheduler() {
        DataLogger.logSchedulerParameters("MAIL",
                        mailSchedulerCorePoolSize,
                        mailSchedulerMaxPoolSize,
                        mailSchedulerKeepAliveTime,
                        mailSchedulerQueueMaxSize);

        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(mailSchedulerQueueMaxSize);
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
                        mailSchedulerCorePoolSize,
                        mailSchedulerMaxPoolSize,
                        mailSchedulerKeepAliveTime,
                        TimeUnit.SECONDS,
                        queue,
                        new CustomizableThreadFactory("mail-scheduler-thread-"));

        return Schedulers.fromExecutor(poolExecutor);
    }

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(mailHost);
        mailSender.setPort(mailPort);

        mailSender.setUsername(mailUsername);
        mailSender.setPassword(mailPassword);

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", transportProtocol);
        props.put("mail.smtp.auth", mailSmtpAuth);
        props.put("mail.smtp.starttls.enable", mailSmtpStartTlsEnable);
        props.put("mail.debug", mailDebug);

        return mailSender;
    }

    @Bean
    public FreeMarkerConfigurationFactoryBean getFreeMarkerConfiguration() {
        FreeMarkerConfigurationFactoryBean configuration = new FreeMarkerConfigurationFactoryBean();
        configuration.setTemplateLoaderPath(templateLocation);
        configuration.setPreferFileSystemAccess(false);
        return configuration;
    }
}
