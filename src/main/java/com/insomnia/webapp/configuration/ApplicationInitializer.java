package com.insomnia.webapp.configuration;

import static com.insomnia.webapp.utils.AppUtils.getAppProperty;
import static javax.servlet.ServletRegistration.Dynamic;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class ApplicationInitializer implements WebApplicationInitializer {
    private static final String DISPATCHER_SERVLET_NAME = "Dispatcher servlet";

    static {
        System.setProperty("log4j.configurationFile", getAppProperty("logging.config"));
        System.setProperty("hibernate.config", getAppProperty("hibernate.config"));
        System.setProperty("application.config", getAppProperty("application.config"));
    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext context = new AnnotationConfigWebApplicationContext();
        context.register(AppContextConfig.class);

        servletContext.addListener(new ContextLoaderListener(context));
        context.setServletContext(servletContext);

        Dynamic servlet = servletContext.addServlet(DISPATCHER_SERVLET_NAME, new DispatcherServlet(context));
        servlet.addMapping("/");
        servlet.setLoadOnStartup(1);
        servlet.setAsyncSupported(true);
    }
}
