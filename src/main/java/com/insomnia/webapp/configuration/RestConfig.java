package com.insomnia.webapp.configuration;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.insomnia.webapp.aop.DataLogger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;
import reactor.core.scheduler.Scheduler;
import reactor.core.scheduler.Schedulers;

@Configuration
@PropertySource("${application.config}")
public class RestConfig {

    @Value("${rest.scheduler.core.pool.size: 2}")
    private int restSchedulerCorePoolSize;
    @Value("${rest.scheduler.max.pool.size: 5}")
    private int restSchedulerMaxPoolSize;
    @Value("${rest.scheduler.keep.alive.time.secs: 60}")
    private int restSchedulerKeepAliveTime;
    @Value("${rest.scheduler.queue.max.size: 100}")
    private int restSchedulerQueueMaxSize;

    @Bean
    @Qualifier("rest-scheduler")
    public Scheduler restCommandScheduler() {
        DataLogger.logSchedulerParameters("REST",
                        restSchedulerCorePoolSize,
                        restSchedulerMaxPoolSize,
                        restSchedulerKeepAliveTime,
                        restSchedulerQueueMaxSize);

        BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(restSchedulerQueueMaxSize);
        ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(
                        restSchedulerCorePoolSize,
                        restSchedulerMaxPoolSize,
                        restSchedulerKeepAliveTime,
                        TimeUnit.SECONDS,
                        queue,
                        new CustomizableThreadFactory("rest-command-scheduler-t-"));

        return Schedulers.fromExecutor(poolExecutor);
    }
}
