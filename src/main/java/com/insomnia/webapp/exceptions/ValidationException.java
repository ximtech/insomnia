package com.insomnia.webapp.exceptions;

import com.insomnia.webapp.common.operationresult.ResponseMessage;

public class ValidationException extends RuntimeException {
    private ResponseMessage responseMessage = ResponseMessage.internalException();

    public ValidationException(ResponseMessage responseMessage) {
        this.responseMessage = responseMessage;
    }

    public ResponseMessage getResponseMessage() {
        return responseMessage;
    }
}
