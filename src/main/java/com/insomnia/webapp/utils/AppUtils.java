package com.insomnia.webapp.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class AppUtils {
    public static final String APPLICATION = "application.properties";

    public static String encryptPassword(String plainPassword) {
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder(16);
        return passwordEncoder.encode(plainPassword);
    }

    public static String getAppProperty(String propKey) {
        try(InputStream inputStream = AppUtils.class.getClassLoader().getResourceAsStream(APPLICATION)) {
            Properties property = new Properties();
            property.load(inputStream);
            return property.getProperty(propKey);

        } catch (IOException e) {
            throw new RuntimeException("Property value is not found", e);
        }
    }

    public static String convertToJsonString(Object responseMessage) {
        ObjectWriter mapper = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            return mapper.writeValueAsString(responseMessage);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "";
    }

}
