package com.insomnia.webapp.services;

import com.insomnia.webapp.controllers.request.CreateNewUserRequest;
import com.insomnia.webapp.controllers.response.VoidResponse;
import com.insomnia.webapp.services.interfaces.RegistrationService;
import com.insomnia.webapp.services.interfaces.mail.EmailService;
import com.insomnia.webapp.services.mail.RegistrationEmailData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private EmailService emailService;

    @Autowired
    public RegistrationServiceImpl(EmailService emailService) {
        this.emailService = emailService;
    }

    @Override
    public VoidResponse getVoidResponse(CreateNewUserRequest request) {
        RegistrationEmailData emailData = new RegistrationEmailData();
        emailData.setSenderEmail(request.getUsername());
        emailData.setUserFirstName(request.getFirstName());
        emailData.setUserLastName(request.getLastName());

        emailService.sendRegistrationConfirmEmail(emailData);
        return new VoidResponse();
    }
}
