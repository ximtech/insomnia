package com.insomnia.webapp.services;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import com.insomnia.webapp.entities.CustomerUser;
import com.insomnia.webapp.security.UserRole;
import com.insomnia.webapp.services.interfaces.SpringSecurityService;
import com.insomnia.webapp.utils.AppUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service    //TODO Implement domain classes
public class UserDetailsServiceImpl implements UserDetailsService, SpringSecurityService {

    private static Map<String, CustomerUser> userMap = new HashMap<>();

    static {
        CustomerUser customerUser = new CustomerUser();
        customerUser.setUsername("colibri");
        customerUser.setPassword(AppUtils.encryptPassword("1234"));
        customerUser.setRole(UserRole.ROLE_USER);

        userMap.put(customerUser.getUsername(), customerUser);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CustomerUser user = getUser(username);

        if (user == null) {
            return null;
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(user.getRole().name()));

        return new User(user.getUsername(), user.getPassword(), grantedAuthorities);
    }

    //TODO must be implemented in DAO layer
    private CustomerUser getUser(String username) {
        return userMap.get(username);
    }

    @Override
    public Optional<User> getCurrentUser() {
        Object user = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return user instanceof User ? Optional.of((User)user) : Optional.empty();
    }

    @Override
    public String getCurrentUserName() {
        Optional<User> currentUser = getCurrentUser();
        return currentUser.isPresent() ? currentUser.get().getUsername() : "<Unknown>";
    }
}
