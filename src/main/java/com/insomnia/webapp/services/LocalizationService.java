package com.insomnia.webapp.services;

import java.util.Locale;

import com.insomnia.webapp.common.operationresult.ResponseMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Service;

@Service
public class LocalizationService {

    private MessageSource messageSource;

    @Autowired
    public LocalizationService(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public String getI18nMessage(String messageKey, String messageLang, String defaultMessage) {
        return getI18nMessage(messageKey, defaultMessage, langToLocale(messageLang));
    }

    public String getI18nMessage(String messageKey, String defaultMessage) {
        return getI18nMessage(messageKey, defaultMessage, LocaleContextHolder.getLocale());
    }

    public String getI18nMessage(String messageKey, String defaultMessage, Locale locale) {
        return getI18nMessage(messageKey, defaultMessage, locale, new Object[0]);
    }

    public String getI18nMessage(String messageKey) {
        return getI18nMessage(messageKey, new Object[0]);
    }

    public String getI18nMessage(String messageKey, Object[] args) {
        return getI18nMessage(messageKey, LocaleContextHolder.getLocale(), args);
    }

    public String getI18nMessage(String messageKey, Locale locale, Object[] args) {
        return getI18nMessage(messageKey, null, locale, args);
    }

    public String getI18nMessage(String messageKey, String defaultMessage, Locale locale, Object[] args) {
        return messageSource.getMessage(messageKey, args, defaultMessage != null ? defaultMessage : "", locale);
    }

    public String getI18nMessage(DefaultMessageSourceResolvable sourceResolvable) {
        return messageSource.getMessage(sourceResolvable, LocaleContextHolder.getLocale());
    }

    public void setLocalizedMessageToResponse(ResponseMessage responseMessage) {
        String localizedMessage = getI18nMessage(responseMessage.getSourceResolvable());
        responseMessage.setMessage(localizedMessage);
    }

    public Locale langToLocale(String messageLang) {
        return new Locale.Builder().setLanguage(messageLang).build();
    }
}
