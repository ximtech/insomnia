package com.insomnia.webapp.services.interfaces;

import java.util.Optional;

import org.springframework.security.core.userdetails.User;

public interface SpringSecurityService {
    Optional<User> getCurrentUser();

    String getCurrentUserName();
}
