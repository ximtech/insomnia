package com.insomnia.webapp.services.interfaces;

import com.insomnia.webapp.controllers.request.CreateNewUserRequest;
import com.insomnia.webapp.controllers.response.VoidResponse;

public interface RegistrationService {
    VoidResponse getVoidResponse(CreateNewUserRequest request);
}
