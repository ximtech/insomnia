package com.insomnia.webapp.services.interfaces.mail;

import java.util.Map;

import com.insomnia.webapp.services.mail.EmailData;

public interface EmailClient {

    void prepareAndSendHTML(EmailData emailData);

    void prepareAndSendHTMLAsync(EmailData emailData);

    void send(EmailData emailData, String content);

    void sendAsync(EmailData emailData, String content);
}
