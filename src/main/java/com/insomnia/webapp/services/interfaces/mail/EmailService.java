package com.insomnia.webapp.services.interfaces.mail;

import com.insomnia.webapp.services.mail.PasswordResetMailData;
import com.insomnia.webapp.services.mail.RegistrationEmailData;

public interface EmailService {

    void sendRegistrationConfirmEmail(RegistrationEmailData emailData);

    void sendPasswordResetEmail(PasswordResetMailData emailData);
}
