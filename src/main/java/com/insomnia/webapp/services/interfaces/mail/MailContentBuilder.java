package com.insomnia.webapp.services.interfaces.mail;

import java.util.Map;
import java.util.Optional;

public interface MailContentBuilder {

    Optional<String> createHTML(Map<String, Object> dataModel, String template);
}
