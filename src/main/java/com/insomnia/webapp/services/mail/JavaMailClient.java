package com.insomnia.webapp.services.mail;

import javax.mail.internet.MimeMessage;

import com.insomnia.webapp.services.interfaces.mail.EmailClient;
import com.insomnia.webapp.services.interfaces.mail.MailContentBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@Service
public class JavaMailClient implements EmailClient {
    private static final Logger LOGGER = LogManager.getLogger(JavaMailClient.class);

    private MailContentBuilder mailContentBuilder;
    private JavaMailSender mailSender;
    private Scheduler mailScheduler;

    @Autowired
    public JavaMailClient(MailContentBuilder mailContentBuilder,
                    JavaMailSender mailSender,
                    @Qualifier("mail-scheduler") Scheduler mailScheduler) {
        this.mailContentBuilder = mailContentBuilder;
        this.mailSender = mailSender;
        this.mailScheduler = mailScheduler;
    }

    @Override
    public void prepareAndSendHTML(EmailData emailData) {
        mailContentBuilder.createHTML(emailData.getContentModel(), emailData.getTemplateName())
                        .ifPresent((String content) -> send(emailData, content));
    }

    @Override
    public void prepareAndSendHTMLAsync(EmailData emailData) {
        Mono.fromRunnable(() -> prepareAndSendHTML(emailData))
                        .publishOn(mailScheduler)
                        .subscribe(null, error -> LOGGER.error("Failed to send mail with subject [{}] to [{}]",
                                        emailData.getSubject(), emailData.getTo(), error));
    }

    @Override
    public void sendAsync(EmailData emailData, String content) {
        Mono.fromRunnable(() -> send(emailData, content))
                        .publishOn(mailScheduler)
                        .subscribe(null, error -> LOGGER.error("Failed to send mail with subject [{}] to [{}]",
                                        emailData.getSubject(), emailData.getTo(), error));
    }

    @Override
    public void send(EmailData emailData, String content) {
        try {
            MimeMessage mimeMessage = mailSender.createMimeMessage();
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, "UTF-8");
            messageHelper.setFrom(emailData.getFrom());
            messageHelper.setTo(emailData.getTo());

            messageHelper.setSubject(emailData.getSubject());
            messageHelper.setText(content, true);

            mailSender.send(mimeMessage);
            LOGGER.debug("Mail with subject [{}] to [{}] was sent!", emailData.getSubject(), emailData.getTo());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
