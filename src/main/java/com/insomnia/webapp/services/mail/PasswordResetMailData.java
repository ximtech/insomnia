package com.insomnia.webapp.services.mail;

public class PasswordResetMailData {
    private String senderEmail;
    private String userFirstName;
    private String userLastName;
    private String link;

    public PasswordResetMailData() {}

    public PasswordResetMailData(String senderEmail,
                    String userFirstName,
                    String userLastName,
                    String link) {
        this.senderEmail = senderEmail;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.link = link;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
