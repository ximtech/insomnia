package com.insomnia.webapp.services.mail;

public class RegistrationEmailData {
    private String senderEmail;
    private String userFirstName;
    private String userLastName;
    private String registrationLink;

    public RegistrationEmailData() {}

    public RegistrationEmailData(String senderEmail,
                    String userFirstName,
                    String userLastName,
                    String registrationLink) {
        this.senderEmail = senderEmail;
        this.userFirstName = userFirstName;
        this.userLastName = userLastName;
        this.registrationLink = registrationLink;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getRegistrationLink() {
        return registrationLink;
    }

    public void setRegistrationLink(String registrationLink) {
        this.registrationLink = registrationLink;
    }
}
