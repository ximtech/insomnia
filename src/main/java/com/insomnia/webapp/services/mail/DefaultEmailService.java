package com.insomnia.webapp.services.mail;

import java.util.HashMap;
import java.util.Map;

import com.insomnia.webapp.services.interfaces.mail.EmailClient;
import com.insomnia.webapp.services.interfaces.mail.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Service
@PropertySource("${application.config}")
public class DefaultEmailService implements EmailService {
    private static final String MAIL_TEMPLATE_NAME = "mail-template.ftl";

    private static final String MODEL_MSG_TEXT = "msgText";
    private static final String MODEL_USER_FIRST_NAME = "userFirstName";
    private static final String MODEL_USER_LAST_NAME = "userLastName";

    @Value("${user.mail.from: arkturmail@gmail.com}")
    private String fromEmail;

    private EmailClient emailClient;

    @Autowired
    public DefaultEmailService(EmailClient emailClient) {
        this.emailClient = emailClient;
    }

    @Override
    public void sendRegistrationConfirmEmail(RegistrationEmailData emailData) {
        Map<String, Object> contentModel = new HashMap<>();
        contentModel.put(MODEL_USER_FIRST_NAME, emailData.getUserFirstName());
        contentModel.put(MODEL_USER_LAST_NAME, emailData.getUserLastName());
        contentModel.put(MODEL_MSG_TEXT, "Registration text");

        EmailData emailSendData = new EmailData();
        emailSendData.setTo(emailData.getSenderEmail());
        emailSendData.setFrom(fromEmail);
        emailSendData.setSubject("Registration subject");
        emailSendData.setTemplateName(MAIL_TEMPLATE_NAME);
        emailSendData.setContentModel(contentModel);

        emailClient.prepareAndSendHTMLAsync(emailSendData);
    }

    @Override
    public void sendPasswordResetEmail(PasswordResetMailData emailData) {

    }
}
