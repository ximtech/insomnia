package com.insomnia.webapp.services.mail;

import java.util.HashMap;
import java.util.Map;

public class EmailData {
    private String to;
    private String from;
    private String subject;
    private String templateName;

    private Map<String, Object> contentModel = new HashMap<>();

    public EmailData() {}

    public EmailData(String to,
                     String from,
                     String subject,
                     String templateName,
                     Map<String, Object> contentModel) {
        this.to = to;
        this.from = from;
        this.subject = subject;
        this.templateName = templateName;
        this.contentModel = contentModel;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Map<String, Object> getContentModel() {
        return contentModel;
    }

    public void setContentModel(Map<String, Object> contentModel) {
        this.contentModel = contentModel;
    }
}
