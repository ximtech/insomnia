package com.insomnia.webapp.controllers.restcontroller;

import com.insomnia.webapp.controllers.request.CreateNewUserRequest;
import com.insomnia.webapp.controllers.response.VoidResponse;
import com.insomnia.webapp.services.interfaces.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Scheduler;

@RestController
@RequestMapping("/registration")
public class RegistrationController {

    private RegistrationService registrationService;
    private Scheduler restScheduler;

    @Autowired
    public RegistrationController(RegistrationService registrationService,
                    @Qualifier("rest-scheduler") Scheduler restScheduler) {
        this.registrationService = registrationService;
        this.restScheduler = restScheduler;
    }

    @PostMapping(path = "/userRegistration")
    public Mono<VoidResponse> registerNewUser(@RequestBody CreateNewUserRequest request) {
        return Mono.fromCallable(() -> registrationService.getVoidResponse(request)).subscribeOn(restScheduler);
    }
}
