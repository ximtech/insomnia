package com.insomnia.webapp.controllers;

import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UserController {

    @Secured("ROLE_USER")
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(Model model) {
        return "index";
    }

    @Secured("ROLE_USER")
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String home(Model model) {
        throw new RuntimeException("TEST ERROR HANDLING");
    }
}
