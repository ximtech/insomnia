package com.insomnia.webapp.controllers.response;

import com.insomnia.webapp.aop.annotations.Response;
import com.insomnia.webapp.common.operationresult.ProcessingResult;

@Response
public class VoidResponse extends ProcessingResult {
}
