package com.insomnia.webapp.controllers.request;

import java.util.StringJoiner;

import com.insomnia.webapp.aop.annotations.BlankFieldValidator;
import com.insomnia.webapp.aop.annotations.Request;
import com.insomnia.webapp.aop.annotations.UserNameValidator;

@Request
public class CreateNewUserRequest {

    @BlankFieldValidator(messageCode = "common.error.firstNameIsInvalid", fieldName = "firstName")
    private String firstName;

    @BlankFieldValidator(messageCode = "common.error.lastNameIsInvalid", fieldName = "lastName")
    private String lastName;

    @UserNameValidator(messageCode = "common.error.usernameIsIncorrect", fieldName = "username")
    private String username;

    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", "CreateNewUserRequest{", "}")
                        .add("firstName='" + firstName + "'")
                        .add("lastName='" + lastName + "'")
                        .add("username='" + username + "'")
                        .toString();
    }
}
