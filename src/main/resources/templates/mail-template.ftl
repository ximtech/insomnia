<!DOCTYPE HTML>
<html style="background: url(cid:images/bg.jpg);">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Insomnia</title>
    <style type="text/css">

        h2,h3,h4,p { color:#000000; }
        table { font-family:Arial; font-size:14px; }

    </style>
</head>

<body bgcolor="#000000" rightmargin="0" leftmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0" offset="0" style="margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; padding-top:0px; padding-right:0px; padding-bottom:0px; padding-left:0px; border:0px;">

<table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#000000" style="background: url(images/bg.jpg); margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; padding-top:0px; padding-right:0px; padding-bottom:0px; padding-left:0px; border:0px;">
    <tr><td valign="top" align="center" style="padding-top:20px; padding-right:0px; padding-bottom:20px; padding-left:0px;">

        <table width="600" cellpadding="0" cellspacing="0" style="font-family:Arial; font-size:14px; color:#000000; padding-top:0px; padding-right:20px; padding-bottom:0px; padding-left:20px;">

            <tr><td align="center" style="padding:20px; text-align:center; font-family:Arial; font-size:14px; color:#000000;">
                <img src="cid:images/logo.png" width="207" height="51" alt="Insomnia Logo" style="padding-top:0px; padding-right:0px; padding-bottom:0px; padding-left:0px; margin-top:0px; margin-right:0px; margin-bottom:0px; margin-left:0px; border:0px;">
            </td></tr>


            <tr><td align="left" bgcolor="#FFFFFF" style="padding-top:0px; padding-right:0px; padding-bottom:0px; padding-left:0px; text-align:left; font-family:Arial; font-size:12px; color:#333333; line-height:22px;">


                <table align="center" width="560" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" >
                    <tr>
                        <td>
                            <p style="padding-top:30px; padding-right:35px; padding-left:35px; text-align:left; font-family:Arial; font-size:14px; color:#333333;">Greeting ${userFirstName} ${userLastName}</p>
                            <p style="padding-top:10px; padding-right:35px; padding-left:35px; text-align:left; font-family:Arial; font-size:14px; color:#333333;">${msgText}</p>
                        </td></tr>


                    <td style="padding-top:40px; padding-right:35px; padding-bottom:40px; padding-left:35px; text-align:left; font-family:Arial; font-size:14px; color:#333333;">
                        <a href="#" style="text-decoration:none; background-color:#8a2121; color:#FFFFFF; padding-top:15px; padding-right:20px; padding-bottom:15px; padding-left:20px; text-align:center; font-family:Arial; font-size:14px;-webkit-border-radius:6px; -moz-border-radius:6px; border-radius:6px; height:40px; width:148px;">Button Example</a>
                    </td></tr>


                </table>
            </td></tr>

            <tr><td align="center" bgcolor="#7C878E" style="padding-top:10px; padding-right:0px; padding-bottom:5px; padding-left:0px;font-family:Times; font-weight:bold; font-size:20px; color:#E6EAEB;">

                <table align="center" width="560" border="0" cellspacing="0" cellpadding="0" bgcolor="#7C878E" >
                    <tr>
                        <td style="padding-top:10px; padding-right:35px; padding-bottom:20px; padding-left:35px; font-family:Arial; font-size:12px; line-height:16px; color:#E6EAEB;">
                            <p style="color:#E6EAEB; font-size:12px; font-weight:normal; font-family:Arial;">
                                Insomnia Industries<br>
                                Latvia, Riga, Ilukstes iela 56-20<br>
                                +37122356745<br>
                                ximtech@inbox.lv<br>
                                <a href="http://www.insomnia.com">www.insomnia.com</a></p>
                        </td>
                    </tr>
                </table>

            </td></tr>
        </table>

    </td></tr></table>



</body>
</html>
