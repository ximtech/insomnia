<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><spring:message code="common.label.title"/></title>

    <script src="webjars/jquery/3.3.1/dist/jquery.slim.min.js"></script>
    <script src="webjars/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="webjars/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <link href="webjars/bootstrap/4.1.3/css/bootstrap.css" rel="stylesheet">

</head>

<body>

<div class="container">

    <div class="jumbotron" style="margin-top: 20px;">
        <h1><spring:message code="common.label.title"/></h1>
        <p class="lead">
            <spring:message code="common.label.body"/>
        </p>

        <br/>

        <div class="dropdown">
            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <spring:message code="common.label.langTitle"/>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="?lang=en"><spring:message code="lang.selector.enLabel"/></a>
                <a class="dropdown-item" href="?lang=ru"><spring:message code="lang.selector.ruLabel"/></a>
            </div>
        </div>

        <br/>

        <form action="/home" method="get">
            <button class="btn btn-lg btn-danger" type="submit"><spring:message code="common.label.errorButton"/></button>
        </form>

        <br/>

        <p><a class="btn btn-lg btn-danger" href="<c:url value="/login"/>" role="button"><spring:message code="common.label.loginOut"/></a></p>

    </div>
</div>

</body>
</html>
